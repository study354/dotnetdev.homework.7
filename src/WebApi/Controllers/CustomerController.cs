using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Servicec;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomerService cs;

        public CustomerController(ICustomerService cs)
        {
            this.cs = cs;
        }


        [HttpGet("{id:long}")]
        public ActionResult<Customer> GetCustomer([FromRoute] long id)
        {
            Customer customer = cs.Get(id);
            if (customer == null)
            {
                return BadRequest("������������ �� ������");
            }
            return Ok(customer);
        }

        [HttpPost("")]
        public ActionResult<long> CreateCustomerAsync([FromBody] Customer customer)
        {
            long? id = cs.Set(customer);
            switch (id)
            {
                case null:
                    return BadRequest("������������ �� ������");
                case -1:
                    return Conflict("����� ������������ ��� ����������");
                default:
                    return Ok(id);
            }
        }
    }
}