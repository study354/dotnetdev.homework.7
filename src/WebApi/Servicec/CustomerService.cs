﻿using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Servicec
{
    public class CustomerService: ICustomerService
    {
        private CustomerDB _db;

        public CustomerService(CustomerDB db)
        {
            _db = db;
        }


        public Customer Get(long id)
        {
            return _db.Customers.FirstOrDefault(c => c.Id == id);
        }

        public long? Set(Customer customer)
        {
            var isCustomer = _db.Customers.Count(c => c.Lastname == customer.Lastname && c.Firstname == customer.Firstname);
            if (isCustomer != 0)
            {
                return  -1;
            }
            _db.Customers.Add(customer);
            _db.SaveChanges();
            return customer.Id;
        }
    }
}
