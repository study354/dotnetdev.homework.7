﻿using WebApi.Models;

namespace WebApi.Servicec
{
    public interface ICustomerService
    {
        public Customer Get(long id);

        public long? Set(Customer customer);
    }
}
