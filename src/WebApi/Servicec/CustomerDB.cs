﻿using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Servicec
{
    public class CustomerDB: DbContext
    {
        public CustomerDB(DbContextOptions options) : base(options) { }
        public DbSet<Customer> Customers { get; set; }
    }
}
