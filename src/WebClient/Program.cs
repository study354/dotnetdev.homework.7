﻿using Bogus;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Net;

namespace WebClient
{
    static class Program
    {
        static readonly HttpClient _httpClient = new ();
        static readonly Faker _faker = new (); 
        static readonly string _url = Environment.GetEnvironmentVariable("url") ?? "https://localhost:5001/customers";

        static async Task Main(string[] args)
        {
            do
            {
                #region Запрос ID с консоли
                Console.WriteLine("Введите ID Клиента, чтобы получить данные пользователя:");
                var input = Console.ReadLine();
                if (long.TryParse(input, out var _))
                {
                    var customer = await GetAsync(input);
                    Console.WriteLine("Данные пользователя из базы: {0}", customer);
                }
                else
                {
                    Console.WriteLine("Введен некорректный ID Клиента.");
                }
                #endregion

                #region Отправка созданного случайного пользователя. Запрос пользователя возвращенному по ID.
                var postResponse = await _httpClient.PostAsJsonAsync(_url, RandomCustomer());
                string idCustomer = await postResponse.Content.ReadAsStringAsync();
                if (long.TryParse(idCustomer, out var _))
                {
                    var cust = await GetAsync(idCustomer);
                    Console.WriteLine("Создан случайный пользователь: {0}", cust);
                }
                #endregion
                Console.WriteLine("Введитие q для выхода:");
                if ("q".Equals(Console.ReadLine())) { break; }
            }
            while (true);
            
        }

        private static CustomerCreateRequest RandomCustomer() =>  new CustomerCreateRequest { Firstname = _faker.Name.FirstName(), Lastname = _faker.Name.LastName()  };

        private static async Task<Customer> GetAsync(string? id)
        {
            var getResponse =  await _httpClient.GetAsync($"{_url}/{id}");
            var resultResponse = await getResponse.Content.ReadAsStringAsync();
            if (getResponse == null || getResponse.StatusCode != HttpStatusCode.OK)
            {
                Console.WriteLine(resultResponse);
                return null;
            }
            return  JsonSerializer.Deserialize<Customer>(resultResponse);
        }
    }   
}